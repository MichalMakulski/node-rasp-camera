;(function() {
  
  var socket = io();
  var btn = document.querySelector('button');
  btn.startStream = false;
  btn.addEventListener('click', function(e) {
    if(btn.startStream) {
      socket.emit('stream', 'stop');
      btn.textContent = 'Start stream';
    } else {
      socket.emit('stream', 'start');
      btn.textContent = 'Stop stream';
    }
    
    btn.startStream = !btn.startStream;
  }, false)
  
  socket.on('newImage', function(data) {
    console.log('new image');
    let img = document.querySelector('img');
    img.src = 'data:image/png;base64,' + data.image;
  })
  
})();