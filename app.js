
const express = require('express');
const app = express();
const spawn = require('child_process').spawn;
const fs = require('fs');
const http = require('http').Server(app);
const io = require('socket.io')(http);
const img = './client/img.jpg';

const temp = {
  req: undefined,
  res: undefined
};
const port = process.argv[2] || 2024;

fs.watchFile(img, (curr, prev) => {
  let stream = fs.createReadStream(img);

  stream.on('data', chunk => {
    let base64Data = chunk.toString('base64');
    io.emit('newImage', {image: base64Data});
  });

});

app.use(express.static('client'));
app.use((req, res, next) => {
  temp.req = req;
  temp.res = res;
  next();
})

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/client/index.html`);
});

io.on('connection', socket => {
  let child;
  console.log('a user connected');
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });
  socket.on('stream', msg => {
    if (msg === 'start') {
      console.log('Stream started...');
      child = spawn('fswebcam', ['-r', '640x480', '/home/pi/camera_sockets/client/img.jpg', '-l', '20']);
      return;
    }
    if (msg === 'stop') {
      console.log('Stream stopped...');
      child.kill('SIGINT');
    }
  })
});

http.listen(port, () => console.log(`App running on port: ${port}...`));